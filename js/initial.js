var	PlayerInit = {

			money: 100,
			life : 20,
			speed: 10, // 10 = fast; 50 = normal mode
			time : 5, // time (in sec) before monsters move
			level: 1,
			score:0,
			bestScore: 0,
		};

towers = [];



//Fonctions pour la difficulté

//Fonctions de notifications

function displayNotif (titre,contenu) {
	//On fait apparaitre le formulaire de game over
	//Désolé si c'est illisible mais je savais pas comment faire autrement xD
	$('.notification').html('<h2>'+ titre + '</h2><p>' + contenu + '</p>').fadeIn();
	$('.notification').click (function(){
		$(this).fadeOut();
	});
};

function displayTuto (){
	var music = new Audio('resources/tuto.mp3');
	music.play();
	var titreTuto = 'Bienvenue dans HOSPITAL PANIC !',
		contenuTuto = 'Le but du jeu, défendre l’organisme des vilains microbes …<br/>Tu auras affaire à des vagues de maladies qui seront de plus en plus puissantes ! <br>(Clique sur ce message pour continuer de lire ce tuto)';

	$('.notification').html('<h2 class ="titreTuto">'+ titreTuto + '</h2>' + '<img src="resources/doctor.png" class="docteur">' + '<p class ="contenuTuto">' + contenuTuto + '</p>');

	$('.notification').click (function(){
		//$('.notification h2, .notification p').fadeOut();
			titreTuto = 'Informations du joueur',
			contenuTuto = "Tu vois les informations en haut à droite ?<br/>Il s'agit de tes infos à toi, oui oui ! Il y a des vies, puis ton argent, puis ton level et enfin le temps restant avant la prochaine vague !<br/>Juste en dessous il y a ta vitesse de jeu : si tu cliques dessus, à la vague suivante, ton jeu se déroulera à la nouvelle vitesse !";

	$('.notification').html('<h2 class ="titreTuto">'+ titreTuto + '</h2>' + '<img src="resources/doctor.png" class="docteur">' + '<p class ="contenuTuto">' + contenuTuto + '</p>');		$('.notification').click (function(){
			
				titreTuto = 'Informations des tours',
				contenuTuto = "Tu vois la tour en bas ? C'est ce qui va te permettre de défendre tes PV ! Clique dessus puis place la où tu veux.<br/>Tu en débloqueras peut-être d'autres si tu survis...";


			$('.notification').html('<h2 class ="titreTuto">'+ titreTuto + '</h2>' + '<img src="resources/doctor.png" class="docteur">' + '<p class ="contenuTuto">' + contenuTuto + '</p>');				$('.notification').click (function(){
					
						titreTuto = 'Informations des monstres',
						contenuTuto = "Les virus vont arriver d'en haut !<br/>Ils seront en vague, et ils avancent plus ou moins vite...<br/>Heureusement qu'ils te donnent de 3 à 8 gold quand tu les as vaincu !<br/>Courage à toi et bonne chance !";

						$('.notification').html('<h2 class ="titreTuto">'+ titreTuto + '</h2>' + '<img src="resources/doctor.png" class="docteur">' + '<p class ="contenuTuto">' + contenuTuto + '</p>');

						$('.notification').click (function(){
						$('.notification h2, .notification p').fadeOut();
						$('.notification').fadeOut();
						music.pause();
						});
				});
		});
	});
}

function notificationWave (Player) {
	$('.notification').fadeOut();
	 var succes = new Audio('resources/towerDebloc.mp3'),
	 	 warning = new Audio('resources/warning.mp3');
	 if (Player.level==5) {
		
		var titre = 'Tour débloquée !',
			contenu = 'Bravo ! Tu as débloqué la Cryothérapie, elle permet de ralentir pendant un court instant les virus :D Par contre elle fait moins mal que les cliniques... Et fais gaffe, tu es niveau 5... J\'espère que tu as lu le tutoriel !';

		displayNotif (titre,contenu);
		succes.play();
	
	} else if (Player.level==10){
		var titre = 'Tour débloquée !',
			contenu = 'Bravo ! Tu as débloqué la Electrothérapie, elle a une chance de rendre confus pendant un court instant les virus :D En revanche elle ne fait pratiquement pas de dégat... Et fais gaffe, tu es niveau 10... J\'espère que tu as lu le tutoriel !';

		displayNotif (titre,contenu);
		succes.play();
	} else if (Player.level==15) {
		var titre = 'Tour débloquée !',
			contenu = 'Bravo ! Tu as débloqué la Thermothérapie, Elle fait très mal... Et fais gaffe, tu es niveau 15... Tu connais la chanson';

		displayNotif (titre,contenu);
		succes.play();
	} else if (Player.level%10==0&&gameOver(Player)!=true) {
		var titre='Un boss et une grosse vague en approche !',
			contenu='Attention ! Un monstre puissant et une grosse vague de monstres va nous envahir !';
			displayNotif(titre,contenu);
			succes.play();
	}  else if (Player.level%5==0&&gameOver(Player)!=true){
		var titre='Une grosse vague en approche !',
			contenu='Attention ! Une grosse vague de monstres va nous envahir !';
			displayNotif(titre,contenu);
			warning.play();
	}

	
}

//Fonctions pour le game over
function gameOver (Player) {
	Player.music.pause();
	Player.music.currentTime = 0;

	if (Player.life<=0) {
		return true;
	} else {
		return false;
	}
};

function score(Player){
	if(Player.score > Player.bestScore){
		Player.bestScore = Player.score;
	}

	$('.score').html('<p> Tu as survécu pendant ' + Player.score + ' vagues! Bien joué à toi, docteur !<br> Meilleur score : ' + Player.bestScore + ' , essaie de le battre.').fadeIn();
};

function displayGameOver () {
	
	var gameover = new Audio('resources/gameover.mp3');
	gameover.play();
	//On fait apparaitre le formulaire de game over
	//Désolé si c'est illisible mais je savais pas comment faire autrement xD
	$('.gameover').html('<form><p>Game over... <br>Voulez vous continuer ?</p><input type="button" name="oui" value="oui"><input type="button" name="non" value="non"></form>').fadeIn();
};



function restart(Player, Parcours, monsters, towers) {
	//Si la personne clique sur oui, cela restart le jeu
	$(":button[value='oui']").click(function() {
  		//console.log('Le joueur veut rejouer');
  		$('.gameover').fadeOut();
  		$('.score').fadeOut();
  		displayTowers(towers);
  		
  		Player.life = 20;
  		Player.level = 1;
  		Player.money = 100;
		Player.time = 5;
		Player.score = 0;
		Player.bestScore;
		
		/* ---------- ---------- */
		/* ------- GAME -------- */
		/* ---------- ---------- */
		makeMonsters(monsters, Parcours, Player);
		// On appelle la fonction qui lance le jeu
		startGame(Player, Parcours, monsters, towers);
	});

	$(":button[value='non']").click(function() {
		//console.log('Le joueur ne veut pas rejouer');
		$('.gameover').html("Merci d'avoir joué !");
	});
};
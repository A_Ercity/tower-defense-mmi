// Fonction jQuery : exécute le jeu une fois que le DOM est chargé
$(function() {

	/* ---------- ---------- */
	/* ----- SETTINGS ------ */
	/* ---------- ---------- */


	// Objet littéral qui stocke l'argent, les vies et la vitesse du jeu
	var	Player = {
			money: 100, 
			life : 20,
			speed: 10, //5 = hardcore; 10 = fast; 50 = normal mode
			time : 60, // time (in sec) before monsters move
			level: 1,
			score : 0,
			bestScore: 0, //A chaque fin de vagues, le score augmente d'1
			music:new Audio('resources/music.mp3'),
		}; 


	/* ---------- ---------- */
	/* ----- PAGE ACCUEIL ------ */
	/* ---------- ---------- */


	var Settings = {
	};
	// On masque le div contenant le jeu
	$('.game, .game-constructor').hide();
	//On attend que l'utilisateur clique sur le bouton "JOUER" pour lancer le jeu : il valide le formulaire
	$("form").on("submit", function (e) {
    	e.preventDefault(); // On ne recharge pas la page avec le formulaire

    	// On récupère le pseudo rentré par l'utilisateur
    	Settings.pseudo = $('input[type=text]#pseudo').val();
		
    	// On récupère le niveau de jeu rentré par l'utilisateur
    	Settings.niveau = $('#niveau-select option:selected').val();
    	
    	// On récupère la vitesse du jeu rentré par l'utilisateur
    	Settings.vitesse = $('#niveau-vitesse option:selected').val();
    	lienAccueilPlayer (Settings, Player);
    	
    	// On lance le jeu
		initial(Settings, monsters, Parcours, Player, towers);
	});

	
	//On fait le lien entre le formulaire et le joueur
	lienAccueilPlayer (Settings, Player);
	

	/* ---------- ---------- */
	/* ------ PARCOURS ----- */
	/* ---------- ---------- */

	// Objet littéral qui stocke le parcours des monstres
	var	Parcours = {
			start: 600, 
			sizeCourse: 150,
			course: [
				['down' ,300],
				['left' ,400],
				['down' ,500],
				['right' ,1500],
				['up', 250]
			]
		};

	// On appelle la fonction qui crée le parcours (visuel)
	makeCourse(Parcours);

	/* ---------- ---------- */
	/* ------ TOWERS ------- */
	/* ---------- ---------- */

	var towers = [];  // Tableau qui stocke toutes les tours du jeu

	// On affiche les tours que l'on peut créer à l'écran
	displayTowers(Player, towers); 

	// On appelle la fonction qui permet de créer des tours
	makeTowers(towers, Player);

	/* ---------- ---------- */
	/* ----- MONSTERS ------ */
	/* ---------- ---------- */

	var	monsters = []; // Tableau qui stocke tous les monstres du jeu
});

// ------------------------------------------------------------------------- //
// ----------------------- ALL FUNCTIONS FOR THE GAME ---------------------- //
// ------------------------------------------------------------------------- //

// ----------------------
// --- FUNCTIONS GAME ---
// ----------------------

// Fonction initial : initialise les variables, lance le décompte et exécute le jeu
function initial(gameSettings, monsters, Parcours, Player, towers) {

	// On affiche le pseudo de l'utilisateur dans le HTML
	$('h2 span.pseudo').text(gameSettings.pseudo);

	// On masque le div contenant le jeu
	$('.game').hide();

	// On masque le div contenant les informations du formulaire
	$('div.accueil').fadeOut();


	displayTuto();


	// On affiche le jeu
	$('div.game, .game-constructor').fadeIn();

	//console.log(gameSettings.pseudo);


	// On appelle la fonction qui permet de créer des monstres
	makeMonsters(monsters, Parcours, Player);

	/* ---------- ---------- */
	/* ------- GAME -------- */
	/* ---------- ---------- */

	// On appelle la fonction qui lance le jeu
	startGame(Player, Parcours, monsters, towers);

}

// Fonction qui déclare les monstres à créer et les stocke dans le tableau des monstres
function makeMonsters(monsters, Parcours, Player) {
	var MonsterToCreate;
	var difficulty=randomMonster(Player);
	var factorDifficulty=1+(difficulty/100);
	// On crée l'ensemble des monstres que l'on stocke dans un tableau
	bossWave(Player, difficulty, monsters, Parcours);
	for (var i = 0, max = nbrWave(Player, difficulty); i < max; i++) {
		waveLevel(Player, difficulty);
		
		// On crée un monstre

		var wave = waveLevel(Player, difficulty),
			hp = wave.hp,
			speed = wave.speed,
			img = wave.img;

		var money;
		if ((hp/100)<3&&Player.level%5!=0) {
			money=3;
		} else if (Player.level%50==0){ 
	   		money=1.5;
	    } else {
			money=(hp/100);
		}
		if (money>8) {
			money=8;
		}
		var death = new Audio('resources/death.mp3');
		MonsterToCreate = new Monster(-100*(i+1), Parcours.start, hp, 'Pikachu', money, img,'',speed, death);
		monsters.push(MonsterToCreate);
		
	}
	
}

//Fonction qui fait augmenter la vitesse du jeu dans un intervalle de trois niveaux de vitesse ( 50 / 10 / 5 )

function changeSpeed(Player){
	
	
	if(Player.speed == 10){
		$('.infos span.speed').on('click', function(){
		Object.defineProperty(Player, 'speed', {
			value : 5,
			writable : true
		})
		$('.infos span.speed').text(Player.speed);
		});
	}
	
	else
	if(Player.speed == 5){
		$('.infos span.speed').on('click', function(){
		Object.defineProperty(Player, 'speed', {
			value : 20,
			writable : true
		})
		$('.infos span.speed').text(Player.speed);
		});
	}
	
	else
	if(Player.speed == 20){
		$('.infos span.speed').on('click', function(){
		Object.defineProperty(Player, 'speed', {
			value : 10,
			writable : true
		})
		$('.infos span.speed').text(Player.speed);
	});
	}
	
}

// Fonction qui lance le jeu
function startGame(Player, Parcours, monsters, towers) {
	
	
	if (Player.level>=2) {
		Player.music.play();
	}
	// On affiche les informations du joueur (html)
	$('.infos span.speed').text(Player.speed);
	$('.infos span.time').text(Player.time);
	$('.infos span.life').text(Player.life);
	$('.infos span.money').text(Player.money.toFixed(2));
	$('.infos span.level').text(Player.level);

	// On lance le décompte
	var timer = setInterval(function() {
		$('.infos span.time').text(Player.time); // On change chaque seconde le temps restant
		if (Player.time <= 0) {
			// On arrête le décompte
			clearInterval(timer);
			
			// On lance le timer pour déplacer les monstres et attaquer
			monsterMove(Player, Parcours, monsters, towers, Player.speed);
		}
		else {
			Player.time--; 
			$('.infos span.time').click (function(){
			Player.time=0;
			
			});
		}
	}, 1000);
}

//Fonction qui fais le lien entre le formulaire et le joueur
function lienAccueilPlayer (Settings, Player) {
	//Niveau du joueur
	switch (Settings.niveau) {
			case 'beginner':
				Player.niveau=Settings.niveau;
				//Mettre l'algorithme niveau facile
				Player
				break;

			case 'normal':
				Player.niveau=Settings.niveau;
				//Mettre l'algorithme niveau normal
				break;

			case 'expert':
				Player.niveau=Settings.niveau;
				//Mettre l'algorithme niveau difficile
				break;
			
		}

	switch (Settings.vitesse) {
			case 'lent':
				Player.speed=20;
				break;

			case 'normal':
				Player.speed=10;
				break;

			case 'rapide':
				Player.speed=5;
				break;
			
		}	
}

//fonction qui chane les waves selon le difficulté choisie
function waveLevel (Player, difficulty) {
	var factorDifficulty=1+(difficulty/100);

	var randomBool = Math.random() >= 0.5; //On crée un random booléen pour modifier ou non les PV des monstres
	
	if ((30 - Player.level)>1) {
		var speedBool = Math.floor(Math.random() * Math.floor((30 - Player.level)));; //On crée un random booléen pour modifier ou non la vitesse des monstres
	} else {
		speedBool = 0;
	}

		switch (Player.niveau) {
			case 'beginner':
				var hp=50*difficulty;
				var speed=0.5;
				if (hp<=750) {
					var img='resources/monstre1.png';
				} else {
					 var img='resources/monstre2.png'
				}
				break;

			case 'normal':
				//Tirage au sort des pv
				if(randomBool){
					var hp=75*difficulty;
					//Tirage au sort de la vitesse
					if(speedBool&&Player.level>3){
						var speed=Player.level*30/hp,
							img='resources/monstre1.png';
					} else if (Player.level<=3) {
						var img='resources/monstre3.png';
						speed=0.5;
					} else {
						var speed=Player.level*100/hp,
							img='resources/monstre2.png'
					}

				} else {
					var hp=30*difficulty;
					var speed=factorDifficulty;
					var img='resources/monstre3.png';
				}

				



				break;

			case 'expert':
				//Tirage au sort des hp
				if(randomBool){
					var hp=80*difficulty;
					//Tirage au sort de la vitesse
					if(speedBool&&Player.level>3){
						var speed=Player.level*40/hp;
						img='resources/monstre1.png';
					} else if (Player.level<=3) {
						var img='resources/monstre3.png';
						speed=0.90;
					} else {
						var speed=Player.level*100/hp,
						img='resources/monstre2.png'
					}

				} else {
					var hp=30*difficulty;
					var speed=factorDifficulty;
					var img='resources/monstre3.png';
				}

				
				break;
			
		}


		

	var array = {
		'hp' : hp,
		'speed' : speed,
		'img': img
	}

		return array;
}

//fonction qui défini les grosses vagues
function nbrWave (Player, difficulty) {
	if (Player.level%5==0) {
		var nbrWave = Player.level*2;
	} else {
		var nbrWave = 2+difficulty;
	}
	return nbrWave;
}

function bossWave (Player, difficulty, monsters, Parcours) {
	var factorDifficulty=1+(difficulty/100);
	





	if (Player.level%10==0) {
		var hp = Player.level*500;
		var money;
		if ((hp/100)<5) {
			money=5;
		} else {
			money=hp/100;
		}
		var death = new Audio('resources/deathBoss.mp3');
		MonsterToCreate = new Monster(-100, Parcours.start, hp, 'Pikachu', 50*factorDifficulty, 'resources/boss.png','',0.10,death);
		monsters.push(MonsterToCreate);
	}
}


// ----------------------
// -- FUNCTIONS OTHERS --
// ----------------------

// Fonction qui calcule l'hypotenuse
function calcHypotenuse(a, b) {
  return(Math.sqrt((a * a) + (b * b)));
}

// Fonction qui retourne une valeur comprise en % d'un chiffre
function hpPourcent (hp, hpMax) {
	return parseInt(hp * 100 / hpMax);
}






